﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagneticField : MonoBehaviour
{

    public GameObject       magnet;
    public float            forceFactor = 10f;
    public float            electrostaticConstant = 0.000000009f;
    public float            charge1 = 1f;
    public float            charge2 = 1f;
    private float           distance;

    private Rigidbody2D     rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log("magnet pos: " + magnet.transform.position);
        // force is applied to this object based on distance from magnet
        rb.AddForce((magnet.transform.position - transform.position) * calculateForceMagnitude());
    }

    private float calculateForceMagnitude() {
        float distance = (magnet.transform.position - transform.position).magnitude;
        // Debug.Log("magnet.pos: " + magnet.transform.position);
        return (electrostaticConstant * charge1 * charge2) / (distance * distance);
    }
}
