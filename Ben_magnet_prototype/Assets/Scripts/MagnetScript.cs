﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagnetScript : MonoBehaviour {

    public GameObject       metalBlock;     // the obj that we will magnetize with
    public LayerMask        layerMask;
    // public float            forceFactor = 10f;
    public float            electrostaticConstant = 0.000000009f;
    public float            charge1 = 1f;
    public float            charge2 = 1f;
    private float           distance;
    
    private Camera      cam;
    private Ray         aimRay;
    private Vector3     mousePos;       // .z will be 0
    private Vector3     origin;         // .z will be 0
    private Vector3     angleReference;
    private Vector3     aimDirection;
    private Vector2     realAimDirection;
    private Transform   aimTransform;
    private float       aimAngle;
    private float       simpleAngle;

    private Rigidbody2D     rb;
    private Rigidbody2D     metalRb;


    // Start is called before the first frame update
    void Start() {
        cam = Camera.main;
        angleReference = Vector3.forward;
        aimTransform = transform.Find("Aim");
        rb = gameObject.GetComponent<Rigidbody2D>();
        metalRb = metalBlock.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        // get the mouse position
        // mousePos = Input.mousePosition;
        mousePos = cam.ScreenToWorldPoint(Input.mousePosition);
        //Debug.Log("mousePos: " + mousePos);

        // get the player's pos
        origin = this.gameObject.transform.position;
        //Debug.Log("origin:" + origin);

        // find the vector connecting the two (only direction matters)
        aimRay = new Ray(origin, mousePos);
        aimDirection = (mousePos - origin).normalized;
        aimAngle = Mathf.Atan2(aimDirection.y, aimDirection.x) * Mathf.Rad2Deg;
        
        //Debug.Log("aimAngle:" + aimAngle);

        // simplify to cardinal directions, set aim direction
        // up
        if (aimAngle > 45f && aimAngle <= 135f) {
            simpleAngle = 90f;
            realAimDirection = Vector2.up;
        }
        //left
        else if ((aimAngle > 135f && aimAngle <= 180f ) || (aimAngle <= -135f && aimAngle > -180f)) {
            simpleAngle = 180;
            realAimDirection = Vector2.left;
        }
        // down
        else if (aimAngle > -135f && aimAngle <= -45f) {
            simpleAngle = -90f;
            realAimDirection = Vector2.down;
        }
        else {
            simpleAngle = 0f;
            realAimDirection = Vector2.right;
        }
        //Debug.Log("Angle, Direction: " + simpleAngle + ", " + realAimDirection);
        
        // rotate reticle to angle
        aimTransform.eulerAngles = new Vector3(0, 0, simpleAngle);
    }

    void FixedUpdate() {
        // Magnetism
        // if metal block collides with ray cast from realAimDirection, then move the player appropriately
        //rb.AddForce((metalBlock.transform.position - transform.position) * 1000 * Time.smoothDeltaTime);    // works
        Vector2 rayOrigin;
        rayOrigin.x = origin.x;
        rayOrigin.y = origin.y;
    

        // RaycastHit hit;
        Debug.DrawRay(rayOrigin, realAimDirection * 100, Color.green);

        // if (Input.GetMouseButton(0) && Physics.Raycast(origin, realAimDirection, out hit, 100f)) { //} && hit.transform.tag == "Metal") {
        //     // Debug.Log("hitting metal box");
        //     Debug.Log("ray is hitting " + hit.transform.name);
        //     Debug.Log("aimDirection: " + realAimDirection);
        //     rb.AddForce((metalBlock.transform.position - transform.position) * calculateForceMagnitude() * Time.smoothDeltaTime);    // works


        // }

        RaycastHit2D hit = Physics2D.Raycast(rayOrigin, realAimDirection, 10, layerMask);
        if (hit.collider != null && hit.transform.tag == "FreeMetal") {
            if (Input.GetMouseButton(0)) {
                //Debug.Log("ray is hitting " + hit.transform.name);
                metalRb.AddForce((metalBlock.transform.position - transform.position) * calculateForceMagnitude() * Time.smoothDeltaTime);    // works
            }    
            if (Input.GetMouseButton(1)) {
                metalRb.AddForce(-(metalBlock.transform.position - transform.position) * calculateForceMagnitude() * Time.smoothDeltaTime);
            }
        }
        else if (hit.collider != null && hit.transform.tag == "Metal") {
            if (Input.GetMouseButton(0)) {
                //Debug.Log("ray is hitting " + hit.transform.name);
                rb.AddForce((metalBlock.transform.position - transform.position) * calculateForceMagnitude() * Time.smoothDeltaTime);    // works
            }    
            if (Input.GetMouseButton(1)) {
                rb.AddForce(-(metalBlock.transform.position - transform.position) * calculateForceMagnitude() * Time.smoothDeltaTime);
            }
        }
        

    }

        












        /*
        RaycastHit hit;
        if (Physics.Raycast(origin, realAimDirection, out hit, 100)) {
            Debug.DrawRay(origin, realAimDirection * hit.distance, Color.red, 0.25f);
        }
        rb.AddForce((transform.position - metalBlock.transform.position) * calculateForceMagnitude());

        // if input to pull...
        if (Input.GetMouseButton(0)) {// && Physics.Raycast(origin, realAimDirection, out hit, 100) && hit.transform.tag == "Metal") {
            //left click
            Debug.Log("Repelling!");
            // if (Physics.Raycast(origin, realAimDirection, out hit, 100)) {
            //     Debug.Log("Ray is cast");
            //     if (hit.transform.tag == "Metal") {
            //         Debug.Log("Hitting box");
            //     }
            // }
            //Debug.Log("magnet pos: " + metalBlock.transform.position);
            // force is applied to this object based on distance from magnet
            
        }

        if (Input.GetMouseButton(1)) {
            // right click
            Debug.Log("Right click");
        }
        //if input to push ...

    */

    private float calculateForceMagnitude() {
        float distance = (metalBlock.transform.position - transform.position).magnitude;
        // Debug.Log("magnet.pos: " + magnet.transform.position);
        //Debug.Log("distance: " + distance);
        return (electrostaticConstant * charge1 * charge2) / (distance * distance);
    }
    
}
