﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player1Script : MonoBehaviour {

	[Header("Set in Inspector")]
	public float			speed = 10f;
	public float			thrust = 10f;
	public float			wallSlideSpeed = 5f;

	private bool			onGround = false;
	private Rigidbody2D		rb2d;


	// Use this for initialization
	void Start () {
		rb2d = gameObject.GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
		
		// Movement is independant of jumping
		// check input for horizontal movement
		float moveDirection = Input.GetAxis("Horizontal");
		// multiply by speed and time
		float moveSpeed = moveDirection * speed * Time.deltaTime;
		// assign pos
		this.transform.Translate(moveSpeed, 0, 0);
	
		//Debug.Log(onGround);
		// jump only if the player is on the ground
		if (onGround && Input.GetKeyDown(KeyCode.Space)) {
			//Debug.Log("jumping");
			// Jump by force
			rb2d.AddForce(transform.up * thrust, ForceMode2D.Impulse);

			// Set onGround false
			onGround = false;
		}	
	}

	void OnCollisionEnter2D(Collision2D collision) {
		if (collision.gameObject.tag == "Ground" || collision.gameObject.tag == "Metal" || collision.gameObject.tag == "FreeMetal") {
			//Debug.Log("Reset jump");
			onGround = true;
		}		
	}

	void OnCollisionStay2D(Collision2D collision) {
		// If player is touching a wall, they move down it slower
		if (collision.gameObject.tag == "Wall") {
			//Debug.Log("Colliding with wall");
			//update y speed 
			float downSpeed = wallSlideSpeed * Time.deltaTime;
			this.transform.Translate(0, -downSpeed, 0);
			onGround = true;
		}
		else if (collision.gameObject.tag == "Ground" || collision.gameObject.tag == "Metal" || collision.gameObject.tag == "FreeMetal") {
			// Do nothing
			onGround = true;
		}
		else if (collision.gameObject.tag != "Player2") {
			onGround = false;
		}
	}

	void OnCollisionExit2D(Collision2D collision) {
		onGround = false;
	}
}
