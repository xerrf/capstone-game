﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class grappler : MonoBehaviour
{
    public KeyCode grapple;
    SpringJoint2D joint;
    Vector3 targetPos;
    RaycastHit2D hit;
    public float dist = 10f;
    public LayerMask mask;
    public GameObject otherPlayer;

    LineRenderer line;
    
    
    // Start is called before the first frame update
    void Start()
    {
        joint = GetComponent<SpringJoint2D>();
        joint.enabled = false;
        line = GetComponent<LineRenderer>();
        line.enabled = false;
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(grapple)){
            
            joint.enabled = true;
            line.enabled = true;
            joint.connectedBody = otherPlayer.GetComponent<Rigidbody2D>();
            
            // targetPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            // targetPos.z = 0;
            // Debug.Log(targetPos);

            // hit = Physics2D.Raycast(transform.position, targetPos - transform.position, dist, mask);
            // if(hit.collider != null && hit.collider.gameObject.GetComponent<Rigidbody2D>() != null){
            //     joint.enabled = true;
            //     line.enabled = true;
            //     joint.connectedBody = hit.collider.gameObject.GetComponent<Rigidbody2D>();
            //     //joint.distance = Vector2.Distance(transform.position, hit.point);
            // }
        }
        if(Input.GetKeyUp(grapple)){
            joint.enabled = false;
            line.enabled = false;
        }
    }
}
