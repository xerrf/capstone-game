﻿using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour {

    public float speed = 10f;
    public KeyCode RightKey;
    public KeyCode LeftKey;
    //public KeyCode freeze;
    //public KeyCode JumpKey;
    
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKey(RightKey)){
            Vector2 currentPos = transform.position;
            Vector2 newPos = new Vector2(currentPos.x + speed * Time.deltaTime, currentPos.y);
            transform.position = newPos;

        }
        if(Input.GetKey(LeftKey)){
            Vector2 currentPos = transform.position;
            Vector2 newPos = new Vector2(currentPos.x - speed * Time.deltaTime, currentPos.y);
            transform.position = newPos;
        }
        // if(Input.GetKeyDown(freeze)){
        //     GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotationX | RigidbodyConstraints2D.FreezeRotationZ | RigidbodyConstraints2D.FreezePositionY | RigidbodyConstraints2D.FreezePositionZ;
        // }
        // if(Input.GetKeyUp(freeze)){
        //     GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None;
        // }
        
    }
}