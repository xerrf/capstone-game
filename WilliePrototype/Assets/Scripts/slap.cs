﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class slap : MonoBehaviour
{
    public GameObject player1;
    public KeyCode hit;
    public float power = 10f;
    private float power2;
    private float cooldown;
    public float range = 2f;

    // Start is called before the first frame update
    void Start()
    {
        power2 = power / 4;
        cooldown = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        cooldown -= Time.deltaTime;
        if(Vector2.Distance(transform.position, player1.transform.position) <= range && cooldown <= 0f){
            if(Input.GetKey(hit)){
                cooldown = 1f;
                player1.GetComponent<Rigidbody2D>().AddForce(player1.transform.up * power);
                if(transform.position.x>= player1.transform.position.x){
                    player1.GetComponent<Rigidbody2D>().AddForce(player1.transform.right * power2 * -1);
                }else{
                    player1.GetComponent<Rigidbody2D>().AddForce(player1.transform.right * power2);
                }
            }
        }    
    }
}
