﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Beam2 : MonoBehaviour
{
    public float _laserBeamLength;
    private LineRenderer _linerenderer;
    // Start is called before the first frame update
    void Start()
    {
        _linerenderer = GetComponent<LineRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButton("Fire1"))
        {
            Vector3 endPosition = transform.position + (transform.right * _laserBeamLength);
            _linerenderer.SetPositions(new Vector3[] { transform.position, endPosition });
        }
        else
        {
            Vector3 endPosition = transform.position;
            _linerenderer.SetPositions(new Vector3[] { transform.position, endPosition });
        }

    }
}