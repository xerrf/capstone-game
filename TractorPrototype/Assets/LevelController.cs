﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelController : MonoBehaviour
{
    public int index;
  
    public GameObject Player1Goal;
   
    public GameObject Player2Goal;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Player1Goal.GetComponent<IsP1InGoal>().IsP1In == 1 && Player2Goal.GetComponent<IsP2InGoal>().IsP2In == 1)
        {
            SceneManager.LoadScene(index);
        }
    }

}
