﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IsP2InGoal : MonoBehaviour
{
    public int IsP2In;
    void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Player2")
        {
            IsP2In = 1;
            //Debug.Log(collision);
            //Debug.Log(IsP2In);
        }
        else
        {
            IsP2In = 0;
        }

    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(IsP2In);
    }
}
