﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player2 : MonoBehaviour
{
    public float speed;

    public Transform p1Location;

    public void TractorMove()
    
    {
        if (Input.GetButton("ControllerFire"))
        {
            Debug.Log("thisworks");
            this.GetComponent<Rigidbody2D>().gravityScale = 0f;
            transform.position = Vector2.MoveTowards(transform.position, p1Location.position, 0.02f);
        }
        else
        {
            return;
        }
        
        //Debug.Log("TractorMoving");
        
    }

    public void TractorStop()
    {
       this.GetComponent<Rigidbody2D>().gravityScale = 2f;
    }

  
    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        this.GetComponent<Rigidbody2D>().gravityScale = 2f;
    }
}
