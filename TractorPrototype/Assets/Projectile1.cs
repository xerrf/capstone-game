﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile1 : MonoBehaviour
{
    public float speed;
    public float lifeTime;
    public float distance;
    public LayerMask tractorTarget;
    public Collider2D temp;
    public bool hit;

    public GameObject destroyEffect;
    // Start is called before the first frame update
    void Start()
    {
        Invoke("DestroyProjectile", lifeTime);
    }

    // Update is called once per frame
    void Update()
    {
        //RaycastHit2D hitInfo = Physics2D.Raycast(transform.position, transform.up, distance, tractorTarget);
        
        /*if (hitInfo.collider != null)
        {
            temp = hitInfo.collider;
            Debug.Log(hitInfo.collider);
            hitInfo.collider.GetComponent<Player2>().TractorMove();
            hit = true;
        }
        else
        {
            //Debug.Log("not floating");
            //hitInfo.collider.GetComponent<GravityOn>().GravityTurnOn();
            
            if(hit == true)
            {
                temp.GetComponent<Player2>().TractorStop();
                hit = false;
            }
        }*/

        
        transform.Translate(Vector2.right * speed * Time.deltaTime);
    }

    void DestroyProjectile()
    {
        //Instantiate(destroyEffect, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }

}
