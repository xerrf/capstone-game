﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IsP1InGoal : MonoBehaviour
{
    public int IsP1In;
    void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Player1")
        {
            IsP1In = 1;
            //Debug.Log(collision);
            //Debug.Log(IsP1In);
        }
        else
        {
            IsP1In = 0;
        }
       
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(IsP1In);
    }
}
