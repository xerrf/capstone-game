﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon1 : MonoBehaviour
{
    public float offset;

    public Transform shotPoint;

    public float distance;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
        float horizontal = Input.GetAxis("RightAnalog_horizontal");
        float vertical = Input.GetAxis("RightAnalog_vertical");
        //Vector3 difference = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
        float rotZ = Mathf.Atan2(vertical, horizontal) * (180 / Mathf.PI);
        transform.rotation = Quaternion.Euler(new Vector3(0, 0, rotZ + offset));

        


        //RaycastHit2D hitInfo = Physics2D.Raycast(shotPoint.position, Camera.main.ScreenToWorldPoint(Input.mousePosition), distance);
        //Debug.DrawRay(shotPoint.position, Input.mousePosition, Color.green);

        int mask = LayerMask.GetMask("Player2Layer");
        

        if (Input.GetButton("ControllerFire"))
        {
            RaycastHit2D hitInfo = Physics2D.Raycast(shotPoint.position, shotPoint.right, distance, mask);
            if (hitInfo == false)
            {
                //hitInfo.collider.GetComponent<Player2>().TractorStop();
                return;
            }
            else if (hitInfo.collider.gameObject.tag == "P2")
            {
                //Debug.Log(hitInfo.transform.name);
                hitInfo.collider.GetComponent<Player2>().TractorMove();
            }
            else
            {
                //.Log(hitInfo);
                //hitInfo.collider.GetComponent<Player2>().TractorStop();
            }
        }
         
    }
}
