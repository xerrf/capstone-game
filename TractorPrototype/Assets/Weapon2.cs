﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon2 : MonoBehaviour
{
    public float offset;

    public Transform shotPoint;

    public float distance;

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Vector3 difference = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
        float rotZ = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0f, 0f, rotZ + offset);


        //RaycastHit2D hitInfo = Physics2D.Raycast(shotPoint.position, Camera.main.ScreenToWorldPoint(Input.mousePosition), distance);
        //Debug.DrawRay(shotPoint.position, Input.mousePosition, Color.green);

        int mask = LayerMask.GetMask("Player1Layer");


        if (Input.GetButton("Fire1"))
        {
            RaycastHit2D hitInfo = Physics2D.Raycast(shotPoint.position, shotPoint.right, distance, mask);
            if (hitInfo == false)
            {
                //hitInfo.collider.GetComponent<Player2>().TractorStop();
                return;
            }
            else if (hitInfo.collider.gameObject.tag == "P1")
            {
                //Debug.Log(hitInfo.transform.name);
                hitInfo.collider.GetComponent<Player1>().TractorMove();
            }
            else
            {
                //Debug.Log(hitInfo);
                //hitInfo.collider.GetComponent<Player2>().TractorStop();
            }
        }

    }
}