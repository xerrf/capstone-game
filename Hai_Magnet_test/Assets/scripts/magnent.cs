﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class magnent : MonoBehaviour
{
    public GameObject player1;

    public GameObject player2;

    public Transform p1;
    public Transform p2;
    
    // Start is called before the first frame update
    void Start()
    {
        p1 = player1.transform;
        p2 = player2.transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.RightShift))
        {
            player1.transform.position = Vector3.MoveTowards(player1.transform.position, p2.position, 0.3f);
            
        }

        if (Input.GetKey(KeyCode.LeftShift))
        {
            player2.transform.position = Vector3.MoveTowards(player2.transform.position,p1.position, 0.3f);
        }

    }
}
