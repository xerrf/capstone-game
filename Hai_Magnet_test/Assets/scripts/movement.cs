﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movement : MonoBehaviour
{
    public float speed = 2.0f;
    public KeyCode moveRight, moveLeft;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(moveRight))
        {
            //move paddle up
            Vector3 currPos = transform.position;
            Vector3 newPos = new Vector3(currPos.x+ speed * Time.deltaTime, currPos.y , currPos.z);
            transform.position = newPos;
        }
        
        if (Input.GetKey(moveLeft))
        {
            //move paddle down
            Vector3 currPos = transform.position;
            Vector3 newPos = new Vector3(currPos.x- speed * Time.deltaTime, currPos.y , currPos.z);
            transform.position = newPos;
        }
    }
}
