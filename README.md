# Capstone Game Folder
Please put files in here as we work and feel free to create relevant folders.

## Git Commands
If you don't want to use the command line, you can download bitbucket or gitkraken app. Or, if you are unfamiliar with git commands, here are the basic ones:
**git status** shows which files have been changed, added, or deleted.
**git add .** adds the changes and gets them ready to be committed. 
**git commit -m"description of changes"** commits the local changes. It is requried to leave a string message with each commit, which is what the -m does. 
**git push** uploads the local changes to the cloud or whatever. Do this after you finish something. 
**git pull** retrieves changes and updates your local files. Do this before working on things.



